![Project Avatar](repo-icon.png)

# Portfolio Website

Repository of the code for my personal website that acts as a portfolio. This repository also deploys my website to AWS.

## Project Structure

```text
portfolio-website
│ README.md
│ CHANGELOG.md
| .gitlab-ci.yml
| .gitignore
| repo-icon.png
```

## Roadmap

This project uses a Trello board called [Website Portfolio](https://trello.com/b/v3xrLTc1/website-portfolio) to track progress, features, and issues.
