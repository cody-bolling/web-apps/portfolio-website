# CHANGELOG

## 0.5

- add short bio to the landing page
- add hover animations and links to gitlab for the project cards

## 0.4

- Polish landing page
- Add projects to the programming projects page
- Clean and document code

## 0.3

- Add initial images and javascript packages

## 0.2

- Add deploy stage and barebone website than can successfully pull dynamodb data

## 0.1

- Initial Commit
